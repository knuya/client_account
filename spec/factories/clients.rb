FactoryBot.define do
  factory :client do
    cpf { CPF.generate }

    trait :complete do
      name { Faker::Name.name }
      email { Faker::Internet.email }
      birth_date { Time.zone.today - 22.years }
      gender { 'F' }
      city { 'Curitiba' }
      state { 'PR' }
      country { 'BR' }
    end
  end
end
