require 'rails_helper'

RSpec.describe Client, type: :model do
  it { is_expected.to have_many :guests }
  it { is_expected.to belong_to(:invited_by).optional }

  context 'encrypted fields' do
    context '#cpf' do
      let(:cpf) { CPF.generate }
      let(:encrypted_cpf) { Crypt.new(cpf).encrypt }
      let(:client) { build(:client, cpf: cpf) }

      it { expect(client.cpf).to eq(cpf) }
      it { expect(client.attributes['cpf']).to_not eq(cpf) }
      it { expect(client.attributes['cpf'].size).to be(encrypted_cpf.size) }
      it { expect(client.attributes['cpf']).to_not eq(encrypted_cpf) }
    end

    context '#name' do
      let(:name) { Faker::Name.name }
      let(:encrypted_name) { Crypt.new(name).encrypt }
      let(:client) { build(:client, name: name) }

      it { expect(client.name).to eq(name) }
      it { expect(client.attributes['name']).to_not eq(name) }
      it { expect(client.attributes['name'].size).to be(encrypted_name.size) }
      it { expect(client.attributes['name']).to_not eq(encrypted_name) }
    end

    context '#email' do
      let(:email) { Faker::Internet.email }
      let(:encrypted_email) { Crypt.new(email).encrypt }
      let(:client) { build(:client, email: email) }

      it { expect(client.email).to eq(email) }
      it { expect(client.attributes['email']).to_not eq(email) }
      it { expect(client.attributes['email'].size).to be(encrypted_email.size) }
      it { expect(client.attributes['email']).to_not eq(encrypted_email) }
    end

    context '#birth_date' do
      let(:birth_date) { 20.years.ago.to_date }
      let(:encrypted_birth_date) { Crypt.new(birth_date.to_s).encrypt }
      let(:client) { build(:client, birth_date: birth_date) }

      it { expect(client.birth_date).to eq(birth_date) }
      it { expect(client.attributes['birth_date']).to_not eq(birth_date) }
      it { expect(client.attributes['birth_date'].size).to be(encrypted_birth_date.size) }
      it { expect(client.attributes['birth_date']).to_not eq(encrypted_birth_date) }
    end
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:cpf) }
    it { is_expected.to validate_uniqueness_of(:referral_code) }
    it { is_expected.to validate_length_of(:country).is_at_least(2).is_at_most(4) }
    it { is_expected.to allow_value('BR', 'DE', 'EUA', 'EEUU').for(:country) }
    it { is_expected.to_not allow_value('País', 'Teste', 'B', 'BRASIL').for(:country) }
    it { is_expected.to validate_length_of(:gender).is_at_most(1) }

    context '#validate_cpf' do
      let(:client) { build(:client, cpf: '00000000000') }

      before { client.valid? }

      it { expect(client).to be_invalid }
      it { expect(client.errors[:cpf]).to include 'invalid' }
    end
  end

  context '#find_or_initialize' do
    let(:cpf) { CPF.generate }
    let(:other_cpf) { CPF.generate }
    let!(:client) { create(:client, cpf: cpf) }

    it { expect(described_class.find_or_initialize_by_cpf(cpf: cpf)).to eq client }
    it { expect(described_class.find_or_initialize_by_cpf(cpf: other_cpf)).to be_a described_class }
    it { expect(described_class.find_or_initialize_by_cpf(cpf: other_cpf)).to_not eq client }

    context 'update cliente data' do
      let(:name) { Faker::Name.name }
      let(:updated) { described_class.find_or_initialize_by_cpf(cpf: client.cpf, name: name) }
      let(:birth_date) { 30.years.ago.to_date }

      it { expect(updated.id).to eq client.id }
      it { expect(updated.name).to eq name }

      context 'with some data' do
        let(:a_new_update) { described_class.find_or_initialize_by_cpf(cpf: updated.cpf, birth_date: birth_date) }

        before { updated.save }

        it { expect(a_new_update.name).to eq name }
        it { expect(a_new_update.birth_date).to eq birth_date }
      end
    end

    context '#complete_form!' do
      let!(:client) { create(:client, cpf: cpf) }

      it { expect(client.reload.status).to eq 'pending' }
      it { expect(client.reload.referral_code).to be_nil }

      context 'full fill' do
        let(:client) { create(:client, name: Faker::Name.name, email: Faker::Internet.email, birth_date: 16.years.ago.to_date, gender: 'M', city: 'Curitiba', state: 'PR', country: 'BR') }

        it { expect(client.reload.status).to eq 'complete' }
        it { expect(client.reload.referral_code).to_not be_nil }
        it { expect(client.referral_code.size).to eq(8)}

        it { expect{client.update(city: 'Pinhais') }.to_not(change { client.referral_code }) }
      end

      context 'with invalid params' do
        let(:client) { create(:client, name: Faker::Name.name, email: Faker::Internet.email, birth_date: 16.years.ago.to_date, gender: 'M', city: 'Curitiba') }

        it { expect(client.reload.referral_code).to be_nil }
        it { expect { client.update(state: 'PR', country: 'Brasil') }.to_not(change { client.referral_code }) }
      end
    end
  end
end
