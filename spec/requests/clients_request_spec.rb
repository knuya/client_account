require 'rails_helper'

RSpec.describe 'Clients', type: :request do
  context '#authenticate' do
    let(:client) { create(:client, :complete) }
    let(:params) { { cpf: client.cpf, referral_code: client.referral_code } }
    let(:json) { JSON.parse(body) }
    let(:invited_cpf) { CPF.generate }
    let(:invited_params) { { cpf: invited_cpf, referral_code: client.referral_code, invited: true } }

    context 'with existent account' do
      before { post '/authenticate', params: params }

      it { expect(json['auth_token'].size).to be > 90 }
    end

    context 'with invited cpf' do
      before { post '/authenticate', params: invited_params }

      it { expect(json['auth_token'].size).to be > 90 }
    end
  end

  context '#create' do
    let(:client) { create(:client, :complete) }
    let(:json) { JSON.parse(body) }
    let(:invited_cpf) { CPF.generate }
    let(:invited_params) { { cpf: invited_cpf, referral_code: client.referral_code, invited: true } }
    let(:token) { json['auth_token'] }

    before { post '/authenticate', params: invited_params }

    context 'complete' do
      let(:params) do
        {
          cpf: invited_cpf,
          name: Faker::Name.name,
          email: Faker::Internet.email,
          birth_date: Time.zone.today - 20.years,
          gender: 'M',
          city: 'Campo Largo',
          country: 'BR',
          state: 'PR'
        }
      end
      let(:header) { { 'Authorization' => "Token #{token}" } }

      it { expect { post '/clients', params: { client: params }, headers: header }.to change{ Client.count }.by(1) }
      it { expect { post '/clients', params: { client: params }, headers: header }.to change{ Client.complete.count }.by(1) }

      context 'creation by parts' do
        let(:params) { { cpf: invited_cpf, name: Faker::Name.name } }

        it { expect { post '/clients', params: { client: params }, headers: header }.to change{ Client.count }.by(1) }
        it { expect { post '/clients', params: { client: params }, headers: header }.to_not(change{ Client.complete.count }) }

        context 'finishing the form' do
          let(:new_params) do
            {
              cpf: invited_cpf,
              email: Faker::Internet.email,
              birth_date: Time.zone.today - 20.years,
              gender: 'M',
              city: 'Pinhais',
              country: 'BR',
              state: 'PR'
            }
          end

          before { post '/clients', params: { client: params }, headers: header }

          it { expect { post '/clients', params: { client: new_params }, headers: header }.to_not(change{ Client.count }) }
          it { expect { post '/clients', params: { client: new_params }, headers: header }.to change{ Client.complete.count }.by(1) }
        end
      end
    end
  end

  context '#guests_list' do
    let(:client) { create(:client, :complete) }
    let(:params) { { cpf: client.cpf, referral_code: client.referral_code } }
    let!(:guests) { create_list(:client, 5, :complete, invited_by: client) }
    let!(:incomplete) { create_list(:client, 6, invited_by: client) }
    let(:json) { JSON.parse(body) }

    before do
      post '/authenticate', params: { cpf: client.cpf, referral_code: client.referral_code }
      get '/clients/guests_list', headers: { 'Authorization' => "Token #{JSON.parse(body)['auth_token']}" }
    end


    it { expect(json['referral_code']).to eq client.referral_code }
    it { expect(json['message']).to eq 'This endpoint only brings the complete status' }
    it { expect(json['related_clients'].size).to eq 5 }
    it { expect(json['related_clients']).to match_array(guests.map { |client| { 'referral_code' => client.referral_code, 'name' => client.name } }) }
  end
end
