class AuthorizeApiRequest
  prepend SimpleCommand

  def initialize(headers = {})
    @headers = headers
  end

  def call
    @client ||= Client.find(decoded_auth_token.dig(:client_id))
  end

  private

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    return @headers['Authorization'].split(' ')[-1] if @headers['Authorization']

    errors.add(:token, 'missing')
  end
end
