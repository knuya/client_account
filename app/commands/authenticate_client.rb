class AuthenticateClient
  prepend SimpleCommand

  def initialize(cpf:, referral_code:, invited: false)
    @cpf = cpf
    @referral_code = referral_code
    @invited = invited
  end

  def call
    JsonWebToken.encode(client_id: referenced.id) if client
  end

  private

  def client
    cpf = @invited ? guest_cpf : referenced.cpf
    return referenced if cpf&.eql?(@cpf)

    errors.add(:client, 'invalid access')
  end

  def guest_cpf
    guest = referenced.guests.find_or_initialize_by_cpf(cpf: @cpf)
    return @cpf if guest.referral_code.blank?

    errors.add(:client, 'invalid referral_code')
  end

  def referenced
    @referenced ||= Client.find_by(referral_code: @referral_code)
  end
end
