class JsonWebToken
  KEY = ENV.fetch('KEY')

  def self.encode(payload, expires_at: 1.hour.from_now)
    payload[:exp] = expires_at.to_i
    JWT.encode(payload, KEY)
  end

  def self.decode(token)
    HashWithIndifferentAccess.new(JWT.decode(token, KEY)[0])
  end
end
