class Crypt
  TOKEN = ENV.fetch('TOKEN')
  SALT = ENV.fetch('SALT')

  attr_reader :value

  def initialize(value)
    @value = value
  end

  def encrypt
    message_encryptor.encrypt_and_sign(value)
  end

  def decrypt
    message_encryptor.decrypt_and_verify(value)
  end

  private

  def message_encryptor
    @message_encryptor ||= ActiveSupport::MessageEncryptor.new(key)
  end

  def key
    key_generator.generate_key(SALT, ActiveSupport::MessageEncryptor.key_len)
  end

  def key_generator
    ActiveSupport::KeyGenerator.new(TOKEN)
  end
end
