module AttrEncrypt
  extend ActiveSupport::Concern

  class_methods do
    def attr_encrypt(*attrs)
      attrs.map do |attr|
        define_method(attr) do
          value = read_attribute(attr)
          return if value.blank?

          decrypted = Crypt.new(value).decrypt
          attr.to_s.include?('date') ? decrypted.to_date : decrypted
        end

        define_method("#{attr}=") do |value|
          return if value.blank?

          super(Crypt.new(value.to_s).encrypt)
        end
      end
    end
  end
end
