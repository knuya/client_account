require 'securerandom'

class Client < ApplicationRecord
  include AttrEncrypt

  attr_encrypt :cpf, :name, :email, :birth_date

  belongs_to :invited_by, class_name: 'Client', optional: true
  has_many :guests, class_name: 'Client', foreign_key: :invited_by_id

  validates_presence_of :cpf
  validate :validate_cpf
  validates_uniqueness_of :referral_code, allow_nil: true
  validates :country, format: { with: /\A[[:upper:]]+\z/ }, length: { maximum: 4, minimum: 2 }, allow_blank: true
  validates_length_of :gender, { maximum: 1 }

  before_save :complete_form!, if: :check_fields

  scope :complete, -> { where(status: 'complete') }

  def self.find_or_initialize_by_cpf(attrs)
    client = filter_by_cpf(attrs.delete(:cpf)) || Client.new(attrs)
    attrs.each do |attr|
      client.public_send("#{attr[0]}=", attr[1]) if attr[1].present?
    end

    client
  end

  def self.filter_by_cpf(cpf)
    Client.all.detect { |client| client.cpf.eql? cpf }
  end

  private

  def complete_form!
    self.status = :complete
    self.referral_code ||= SecureRandom.alphanumeric(8)
  end

  def check_fields
    %w[name email birth_date gender city state country].each do |attr|
      return false unless attributes[attr].present?
    end

    true
  end

  def validate_cpf
    errors.add(:cpf, 'invalid') unless CPF.valid?(cpf, strict: true)
  end
end
