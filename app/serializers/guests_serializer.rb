class GuestsSerializer < ActiveModel::Serializer
  attributes :referral_code, :message

  has_many :related_clients

  def message
    'This endpoint only brings the complete status'
  end

  def related_clients
    guests.map do |client|
      { referral_code: client.referral_code, name: client.name }
    end
  end

  private

  def guests
    object.guests.complete
  end
end
