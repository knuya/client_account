class ApplicationController < ActionController::API
  before_action :authenticate_request

  private

  def authenticate_request
    render json: { error: 'Unauthorized' }, status: 401 unless current_client
  end

  def current_client
    AuthorizeApiRequest.call(request.headers).result
  end
end
