class AuthorizationController < ApplicationController
  skip_before_action :authenticate_request

  def authenticate
    command = AuthenticateClient.call(login_params)

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def login_params
    { cpf: params[:cpf], referral_code: params[:referral_code], invited: params[:invited] }
  end
end
