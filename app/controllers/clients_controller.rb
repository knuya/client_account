class ClientsController < ApplicationController
  before_action :load_client, only: :create

  def create
    if @client.update(client_params)
      render json: ClientSerializer.new(@client).to_json, status: 200
    else
      render json: { error: @client.errors }, status: 400
    end
  end

  def guests_list
    render json: GuestsSerializer.new(current_client).to_json, status: 200
  end

  private

  def client_params
    params.require(:client).permit(:cpf, :name, :email, :birth_date, :gender, :city, :state, :city, :state, :country)
  end

  def load_client
    @client = current_client.guests.find_or_initialize_by_cpf(client_params)
  end
end
