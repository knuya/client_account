# README

Aplicação API para cadastro de clientes, com dados sensiveis criptografados.

- Ruby: 2.6.5

- Rails: 6.0.2

- Teste unitários: rspec

Utilizei o ActiveSupport:MessageEncryptor para criptografar, mas como falo abaixo possui falhas para buscas.
Para a construção da API utilziei JWT, com base em uma validação simples de CPF/Código de referência
Foquei nos testes do `Client` para garantir a consitência dos dados trabalhados na API.
Para o relacionamento entre o cliente e seus convidados fiz uma relação `nested` simples.
Nas validações para facilitar utilizei a gem `cpf_cnpj`, também nos testes o `shoulda-matchers`

## Regras
1. Cliente só pode se cadastrar por convite (pela API)
2. Autenticação por convite somente para a criação, após estar completa todos os endpoints só são acessiveis por seu próprio código de referência

## Pontos de melhoria

Melhorar a busca de clientes por CPF, por hora enquanto a base é pequena não será um problema.
Próximos passos, alterar a forma de criptografia do CPF para permitir buscas e deixar menos custoso, ou utilizar alguma outra metodologia de busca. Apesar de usar na busca atual o `detect` e trazer logo que o localiza ainda é um algoritmo muito custoso assintoticamente, somente na busca é um O(n) e isso que não contabiliza que o objeto é instanciado no momento da busca.

Melhorar também as validações, alguns campos ainda ficaram sem validação e os que possuem algum ainda são falhas permitindo dados incorretos serem inseridos.

Melhorar segurança do login.

Melhorar lógica dos serializers e adicionar mais testes para o projeto.

## Endpoints

- POST `/authenticate`

Usado para receber o token e seguir o uso da API

Exemplo de entrada:

`params { cpf: '13410224203', referral_code: 'wJlUoKOT'  }`


Exemplo de resposta:


`{ "auth_token": "eyJhbGciOiJIUzI1NiJ9.eyJjbGllbnRfaWQiOjEsImV4cCI6MTU4OTA4ODIwMX0.Vnk8v0kFhyoJhfdNhzcjpUH-hLeZgY4tBQo2qp3tAEs" }`

- POST `/clients`
Cria o cliente (atualmente o endpoint somente funciona para usuários convidados)
Exemplo de entrada:

`params { cpf: '46412185879', email: 'jody_hackett@kling.biz', gender: 'M', birth_date '2000-05-10', city: 'Campo Largo', state: 'PR', country: 'BR' }
`
Exemplo de resposta:

`{
  "cpf": "46412185879",
  "email": "jody_hackett@kling.biz",
  "gender": "M",
  "birth_date": "2000-05-10",
  "city": "Campo Largo",
  "state": "PR",
  "country": "BR",
  "referral_code": "oZqAC0aI"
}
`

- GET `/clients/guests_list`
Lista os clientes cadastrados por referência com status completo.

Exemplo de resposta:

`{
  "referral_code": "efpf3B5L",
  "message": "This endpoint only brings the complete status",
  "related_clients": [
    {
      "referral_code": "2R9Q1RiU",
      "name": "Robt Collier"
    },
    {
      "referral_code": "mstmcz9U",
      "name": "Dr. Jeannette Rosenbaum"
    },
    {
      "referral_code": "ScU0gWVi",
      "name": "Tuan McGlynn"
    },
    {
      "referral_code": "bsAtHfmd",
      "name":"Charis Erdman"
    },
    {
      "referral_code": "zkQuxw4J",
      "name":"Miss Jani Kub"
     }
  ]
}
`
