class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :email
      t.string :cpf
      t.string :birth_date
      t.string :gender, limit: 1
      t.string :city
      t.string :state, limit: 2
      t.string :country, limit: 4
      t.string :referral_code
      t.references :invited_by
      t.string :status, default: 'pending'
      t.index :cpf
      t.index :referral_code
      t.timestamps
    end
  end
end
