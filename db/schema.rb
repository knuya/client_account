# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_05_014253) do

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "cpf"
    t.string "birth_date"
    t.string "gender", limit: 1
    t.string "city"
    t.string "state", limit: 2
    t.string "country", limit: 4
    t.string "referral_code"
    t.integer "invited_by_id"
    t.string "status", default: "pending"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cpf"], name: "index_clients_on_cpf"
    t.index ["invited_by_id"], name: "index_clients_on_invited_by_id"
    t.index ["referral_code"], name: "index_clients_on_referral_code"
  end

end
