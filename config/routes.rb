Rails.application.routes.draw do
  post 'authenticate', to: 'authorization#authenticate'

  resources :clients, only: [:create] do
    get :guests_list, on: :collection
  end
end
